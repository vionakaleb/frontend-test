import React, { useState, useRef } from "react"
import {
  Card,
  CardBody,
} from "reactstrap"
import Breadcrumbs from "../../../components/Common/Breadcrumb"
import Datatable from "../../../components/Datatable/Datatable"
import Avatar from 'react-avatar';

const TablePage = () => {
  const [columns, setColumns] = useState([
    {
      name: 'Username',
      width: "250px",
      selector: row => row && row.login && row.login.username ?
        <div title={row.login.username}>
          <Avatar round color="#1976d2" size="30" name={row.login.username} />
          {" "}{row.login.username}
        </div>
        : "-"
    },
    {
      name: 'Name',
      width: "200px",
      selector: row => row && row.name && <div>{row.name.title} {row.name.first} {row.name.last}</div>,
    },
    {
      name: 'Email',
      width: "200px",
      selector: row => <div>{row.email}</div>,
    },
    {
      name: 'Gender',
      width: "120px",
      selector: row => <div title={row.gender}>{row.gender}</div>,
    },
    {
      name: 'Registered Date',
      width: "200px",
      selector: row => row && row.registered && row.registered.date ? 
        <div title={row.registered.date}>{row.registered.date}</div>
        : "-"
    },
  ]);

  const [totalRow, setTotalRow] = useState(0);
  const [perPage, setPerPage] = useState(10);

  const [ SearchValue, setSearchValue ] = useState("")
  const _datatable = useRef();

  return (
    <React.Fragment>
      <div className="page-content">
        <Breadcrumbs title="Menu" breadcrumbItem="Table" />

        <Card>
          <CardBody>
            <div className="d-flex flex-row justify-content-end">
              <input
                type="text"
                className="form-control w-25"
                placeholder="Search..."
                onKeyUp={(val) => setSearchValue(val.target.value)}
              />
              <span/>
            </div>

            <Datatable
              columns={columns}
              endpoint="api"
              SearchValue={SearchValue}
              ref={_datatable}
              totalRow={totalRow}
              perPage={perPage}
            />
          </CardBody>
        </Card>
      </div>
    </React.Fragment>
  )
}

export default TablePage
